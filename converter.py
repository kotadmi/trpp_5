def celsius_fahrenheit(c):
    f = (9 / 5.0 * c) + 32
    return round(f, 2)


def celsius_kelvin(c):
    k = c + 273.15
    return round(k, 2)


def fahrenheit_celsius(f):
    c = 5.0 * (f - 32) / 9
    return round(c, 2)


def fahrenheit_kelvin(f):
    k = (f + 459.67) * 5 / 9
    return round(k, 2)


def kelvin_celsius(k):
    c = k - 273.15
    return round(c, 2)


def kelvin_fahrenheit(k):
    f = 1.8 * (k - 273.15) + 32
    return round(f, 2)


def convert(v, x, y):
    with open('test.txt', 'a') as the_file:
        try:
            value = float(v)
        except ValueError:
            the_file.write(f'{v} is not a number!\n')
            return 'error'
        # print(x)
        if x != 'c' and x != 'k' and x != 'f':
            the_file.write(f'Wrong first scale!\n')
            return 'error'
        if y != 'c' and y != 'k' and y != 'f':
            the_file.write(f'Wrong second scale!\n')
            return 'error'

        if x == 'c':
            if y == 'f':
                result = celsius_fahrenheit(value)
            if y == 'k':
                result = celsius_kelvin(value)
            if y == 'c':
                result = value

        if x == 'f':
            if y == 'c':
                result = fahrenheit_celsius(value)
            if y == 'k':
                result = fahrenheit_kelvin(value)
            if y == 'f':
                result = value

        if x == 'k':
            if y == 'f':
                result = kelvin_fahrenheit(value)
            if y == 'c':
                result = kelvin_celsius(value)
            if y == 'k':
                result = value

        the_file.write(f'{value} {x} = {result} {y}\n')

    return result

