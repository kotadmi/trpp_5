from converter import convert

def test_celsius_kelvin():
    assert convert(15, 'c', 'k') == 288.15


def test_celsius_kelvin2():
    assert convert(14.85, 'c', 'k') == 288.0 


def test_celsius_fahrenheit():
    assert convert(17, 'c', 'f') == 62.60


def test_celsius_fahrenheit2():
    assert convert(56.8, 'c', 'f') == 134.24


def test_fahrenheit_celsius():
    assert convert(77, 'f', 'c') == 25.00


def test_fahrenheit_celsius2():
    assert convert(-53.6, 'f', 'c') == -47.56


def test_fahrenheit_kelvin():
    assert convert(32, 'f', 'k') == 273.15


def test_fahrenheit_kelvin2():
    assert convert(380.93, 'f', 'k') == 467.00


def test_kelvin_celsius():
    assert convert(288, 'k', 'c') == 14.85


def test_kelvin_celsius2():
    assert convert(288.15, 'k', 'c') == 15.00


def test_kelvin_fahrenheit():
    assert convert(25, 'k', 'f') == -414.67


def test_kelvin_fahrenheit2():
    assert convert(446.5, 'k', 'f') == 344.03

def test_error_value():
    assert convert('hello', 'f', 'k') == 'error'


def test_error_x():
    assert convert(25, 'hello', 'k') == 'error'


def test_error_y():
    assert convert(125, 'f', 'hello') == 'error'
